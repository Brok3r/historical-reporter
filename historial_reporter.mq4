#include <hash.mqh>
#include <json.mqh> // https://www.mql5.com/es/code/11134
//+------------------------------------------------------------------+
//|                                           historial_reporter.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      ""
#property version   "1.00"
#property strict
//--- input parameters
enum ENUM_MODES {
mode_account_history =MODE_HISTORY,
mode_makert_and_pending = MODE_TRADES
};

input string   url = "https://optimizator.herokuapp.com/api/trader/1/portfolio/05e6f04e-be05-41b8-96a1-10d08d080818/";
//input string   url = "http://5e9ebe2e.ngrok.io/api/trader/1/portfolio/d4cc9de0-66a0-4713-90bf-bf6d21a42be3/";

input int timeout = 5000;
input int time_interval_seconds = 3;
input ENUM_MODES modo=mode_makert_and_pending;

string headers = "Content-Type: application/json";
string strResult,result_header;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- create timer
   EventSetTimer(time_interval_seconds); // number of seconds
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//--- destroy timer
   EventKillTimer();
      
  }

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
  {
   operaciones();
  }
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester()
  {
//---
   double ret=0.0;
//---

//---
   return(ret);
  }
//+------------------------------------------------------------------+

void send_order(){

string order_string = StringConcatenate("{",
                                                         "\"number\":",
                                                            OrderTicket(),
                                                                ",\"open_time\":\"",
                                                            OrderOpenTime(),
                                                                "\",\"order_type\":",
                                                            OrderType(),
                                                               ",\"lots\":\"",
                                                            OrderLots(),
                                                               "\",\"symbol\":\"",
                                                                OrderSymbol(),
                                                                    "\",\"open_price\":\"",
                                                             DoubleToString(OrderOpenPrice(),5),
                                                          "\",\"stop_loss\":\"",
                                                             DoubleToString(OrderStopLoss(),5),
                                                              "\",\"take_profit\":\"",
                                                                DoubleToString(OrderTakeProfit(),5),
                                                                 "\",\"close_time\":\"",
                                                                OrderCloseTime(),
                                                              "\",\"close_price\":\"",
                                                               DoubleToString(OrderClosePrice(),5),
                                                                  "\",\"commission\":\"",
                                                                   DoubleToString(OrderCommission(),2),
                                                              "\",\"swap\":\"",
                                                              DoubleToString(OrderSwap(),2),
                                                                 "\",\"profit\":\"",
                                                                 DoubleToString(OrderProfit(),2),
                                                                "\",\"comment\":\"",
                                                                 OrderComment(),
                                                               "\",\"magical_number\":",
                                                            OrderMagicNumber(),
                                                               ",\"expiration\":\"",
                                                            OrderExpiration(),
                                                            "\",\"balance\":\"",
                                                            DoubleToString(AccountBalance(),2),
                                                            "\"",
                                                             "}"
                                                         );
Print(order_string);

JSONParser *parser = new JSONParser();

JSONValue *jv = parser.parse(order_string);
    if (jv == NULL) {
        Print("Parse json. Error code  =",GetLastError());
        Print("error:"+(string)parser.getErrorCode()+parser.getErrorMessage());
    } else {
       Print("PARSED:"+jv.toString());
       }
       
char res;
char data[], result[];
       
// 4060 ERR_FUNCTION_NOT_CONFIRMED (Function is not allowed for call) --> Tools>options>expert advisor> add url domain
StringToCharArray(order_string, data, 0, StringLen(order_string));
ResetLastError();

//res = WebRequest("POST", url, headers, timeout, data, data, headers);
res = WebRequest("POST", url, headers, timeout, data, result, result_header);

Sleep(10000);
int res_array[];
if(res==-1)
            {
             Print("Error in WebRequest. Error code  =",GetLastError());
             //--- Perhaps the URL is not listed, display a message about the necessity to add the address
             Print("Add the address 'http://optimizator.herokuapp.com/'  in the list of allowed URLs on tab 'Expert Advisors'","Error",MB_ICONINFORMATION);
            }
          else
           {
               for(int i=0;i<ArraySize(result);i++)
               {

                   if( (result[i] == 10) || (result[i] == 13)) {
                      continue;
                   } else {
                      strResult += CharToStr(result[i]);
                   }
               }
               ArrayCopy(res_array,result,0,0,WHOLE_ARRAY);

           }
           Print(strResult);
 
}


void operaciones(){
   int numero_opreaciones = OrdersHistoryTotal();
   bool orden = False;
   for(int i=0; i<numero_opreaciones; i++){
      orden = OrderSelect(i, SELECT_BY_POS, modo); // MODE_TRADES order selected from trading pool(opened and pending orders)
      if(orden) {
       send_order();
      }
   }


}
